#!/usr/bin/python3

import json
from pymongo import MongoClient
from bson.json_util import dumps
from flask import Flask, jsonify, make_response, redirect, request, render_template
from blueprint.site import site
from datetime import datetime

app = Flask(__name__)
app.register_blueprint(site)

client = MongoClient()

db = client.segunda
#for u in db.usuarios.find():
#    print(u)

@app.route('/')
def home():
    #return 'Rodando...'
    usuarios = []
    for u in db.usuarios.find():
        del u['_id']
        usuarios.append(json.loads(dumps(u)))
    return jsonify(usuarios)

@app.route('/cadastrar' , methods=['POST'])
def bunda():
    dados = request.get_json()
    if dados is None:
       return make_response(jsonify({'Mensagem' : 'Corpo não pode ser vazio'}), 400)
    dados['labetina'] = 'fumacinha'
    return jsonify(dados)
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)

