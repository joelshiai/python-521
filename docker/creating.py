#!/usr/bin/python3
import requests
from docker import DockerClient

DOCKER_URL = 'tcp://127.0.0.1:2376'
client = DockerClient(base_url=DOCKER_URL, version='auto')

try:
    client.containers.get('apache').remove(force=True)
except:
    pass

client.containers.run('httpd:alpine', detach=True, ports={80 : 10000}, name='apache', environment={'NOME' : 'Joel Oliveira'})

r = requests.get('http://127.0.0.1:10000')

if r.status_code == 200:

   print('Rodando')
