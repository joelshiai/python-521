#!/usr/bin/python3

import docker

DOCKER_URL = 'tcp://127.0.0.1:2376'
client = docker.DockerClient(base_url=DOCKER_URL, version='auto')

for c in client.containers.list(all=True):
        print('{0} - {1}'.format(c.name, c.image.tags[0]))
